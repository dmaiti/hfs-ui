import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HfsCommonModule } from './hfs-common/hfs-common.module';
import { HfsSettingsModule } from './hfs-settings/hfs-settings.module';
import { HttpClientModule } from '@angular/common/http';
import { HfsBootstrapModule } from './hfs-bootstrap/hfs-bootstrap.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HfsCommonModule,
    HfsSettingsModule,
    HttpClientModule,
    HfsBootstrapModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
