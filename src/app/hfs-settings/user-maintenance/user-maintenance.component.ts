import { Component, OnInit } from '@angular/core';
import { User } from '../users/user-entity';
import { UserMaintenance } from '../users/user-maintenance-entity';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-maintenance',
  templateUrl: './user-maintenance.component.html',
  styleUrls: ['./user-maintenance.component.css']
})
export class UserMaintenanceComponent implements OnInit {

  users : User[];
  usersMaintenance : UserMaintenance[] = [];
  selectedUser : User;
  selectedUserMaintenance : UserMaintenance;
  len : number ;
  returnResponse : any;

  constructor(private userService : UserService,private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.fetchAllUser();
  }

  fetchAllUser(){

    this.userService.getAllUser().subscribe((response) => {
      this.users = response;
      for(let i=0;i<this.users.length ;i++){  
        let userMaintenance = new UserMaintenance();

        userMaintenance.userName = this.users[i].userName;
        userMaintenance.password = this.users[i].password;
        userMaintenance.displayName = this.users[i].displayName;
        userMaintenance.accessType = this.users[i].accessType;
        userMaintenance.profileId = this.users[i].profileId;
        userMaintenance.isEditable = false;
        this.usersMaintenance.push(userMaintenance);
      }
    });
  }

  onSelect(user: UserMaintenance): void {
    user.isEditable = true;
    this.selectedUserMaintenance = user;
  }

  deleteUser(){
    this.selectedUser = new User();
    this.selectedUser.userName = this.selectedUserMaintenance.userName;

    this.userService.deleteUser(this.selectedUser);
    
    let index = this.usersMaintenance.indexOf(this.selectedUserMaintenance);
    this.usersMaintenance.splice(index,1);

  }

  updateUser(user : UserMaintenance) {
    console.log("Before calling userMaintenance : " + this.selectedUserMaintenance.profileId);
    this.selectedUser = new User();
    this.selectedUser.userName = this.selectedUserMaintenance.userName;
    this.selectedUser.password = this.selectedUserMaintenance.password;
    this.selectedUser.displayName = this.selectedUserMaintenance.displayName;
    this.selectedUser.accessType = this.selectedUserMaintenance.accessType;
    this.selectedUser.profileId = this.selectedUserMaintenance.profileId;
    console.log("Before calling update : " + this.selectedUser.profileId);

    this.userService.saveUser(this.selectedUser).subscribe((response) => {this.returnResponse=response});

    let index = this.usersMaintenance.indexOf(this.selectedUserMaintenance);
    this.usersMaintenance.splice(index,1);
    this.selectedUserMaintenance.isEditable = false;
    this.usersMaintenance.push(this.selectedUserMaintenance);

    //location.reload();

  }

}
