import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { FormsModule} from '@angular/forms';
import { UserMaintenanceComponent } from './user-maintenance/user-maintenance.component';
import { HfsBootstrapModule } from '../hfs-bootstrap/hfs-bootstrap.module';
import { CategoryComponent } from './category/category.component';
import { UserService } from './user.service';
import { CategoryService } from './category.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HfsBootstrapModule
  ],
  declarations: [UsersComponent, UserMaintenanceComponent, CategoryComponent],
  exports : [UsersComponent,UserMaintenanceComponent],
  providers : [UserService,CategoryService]
})
export class HfsSettingsModule { }
