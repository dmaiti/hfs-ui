export class UserMaintenance {
	userName : string;
    password : string;
    displayName : string;
    accessType : string;
    profileId : number;
    isEditable : boolean;
}