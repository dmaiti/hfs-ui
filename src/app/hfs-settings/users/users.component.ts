import { Component, OnInit } from '@angular/core';
import { User } from './user-entity';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  userName : string = "";
  password : string = "";
  reenterPassword : string ="";
  displayName : string ="";
  accessType : string = "";
  statusMessage : String = "";
  profileId : number;
  responseCode : string;

  userObj : User ;
  returnResponse : any ;

  constructor(private userService : UserService) { }

  ngOnInit() {

  }

  createUser(){

    this.userObj = new User();
    this.userObj.userName = this.userName;
    this.userObj.password = this.password;
    this.userObj.displayName = this.displayName;
    this.userObj.accessType = this.accessType;
    this.userObj.profileId = 1; // Need to change after with Profile/New customer set up functionality
    console.log(this.userObj);

    this.userService.saveUser(this.userObj).subscribe(
      
      response => {
        this.returnResponse=response
        this.userName = "";
        this.password = "";
        this.reenterPassword = "";
        this.displayName = "";
        this.accessType = "";
        this.responseCode = "SUCCESS"
        this.statusMessage = "Congratulation !!!! User " + this.userObj.userName + " created successfully. ";
      },

      err => {
        console.log(err);
        this.responseCode = "ERROR"
        this.statusMessage = "Sorry!!!! Usre " + this.userObj.userName + " is already taken. ";
      }

    );
  }

}
