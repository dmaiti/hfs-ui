export class Category {
    categoryId : number;
    categoryName : string;
    expenseType : string;
    monthlyBudget : number;
    profileId : number;
}
