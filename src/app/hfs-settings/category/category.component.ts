import { Component, OnInit } from '@angular/core';
import { Category } from './category-entity';
import { User } from '../users/user-entity';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categoryName : string;
  expenseType : string = "";
  monthlyBudget : number;
  categoryObj : Category;
  statusMessage : string;
  responseCode : string;

  constructor(private categoryService : CategoryService) { }

  ngOnInit() {
  }

  createCategory(){
   this.categoryObj = new Category();
   this.categoryObj.categoryName = this.categoryName;
   this.categoryObj .expenseType = this.expenseType;
   this.categoryObj .monthlyBudget = this.monthlyBudget;
  
   this.categoryService.saveCategory(this.categoryObj).subscribe(
     response => {
        this.categoryName = "";
        this.expenseType = "";
        this.monthlyBudget = 0;
        this.responseCode = "SUCCESS";
        this.statusMessage = "New category created successfully";
        console.log(response);
      },
      err => {
        this.responseCode = "ERROR";
        this.statusMessage = "Error occured while setting this Category";
        console.log(err);
      }
     
     );

   this.categoryName = "";
   this.expenseType = "";
   this.monthlyBudget = 0;

   

  }
}
