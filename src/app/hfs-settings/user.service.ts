import { Injectable, Inject } from '@angular/core';
import { User } from './users/user-entity';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Subject} from 'rxjs'
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  observableUser : Observable<User>;
  loggedInUser : User;
  allUserObservable : Observable<User[]>;
  allUsers : User[];
 
  public userSubject = new Subject<User>();

  constructor(@Inject(SESSION_STORAGE) private storage:StorageService,private http : HttpClient) {
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
   }

  saveUser(inputUser : User){
    console.log(inputUser);
    return this.http.post('http://localhost:8080/hfs/settings/saveUser',inputUser);
  }

  deleteUser(inputUser : User){
    this.http.post('http://localhost:8080/hfs/settings/deleteUser',inputUser).subscribe(
      () => {console.log(inputUser.userName + " deleted .")}
    );
  }
  
  getAllUser() : Observable<User[]>{
    let inputUser = new User();
    inputUser.profileId = this.loggedInUser.profileId;
    this.allUserObservable = this.http.post<User[]>('http://localhost:8080/hfs/settings/users',inputUser);
    this.allUserObservable.subscribe((data) => { this.allUsers = data});
    //this.userSubject.next(this.allUsers);
    return this.allUserObservable;
  }

  validateUser(inputUser : User) : Observable<User> {
    this.observableUser =  this.http.post<User>('http://localhost:8080/hfs/settings/login',inputUser);
    this.observableUser.subscribe((response) => { this.loggedInUser = response}) ;
    //this.userSubject.next(this.loggedInUser);
    return this.observableUser;
  }
 
}