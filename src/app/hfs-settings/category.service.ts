import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from './category/category-entity';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { User } from './users/user-entity';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  loggedInUser : User;

  constructor(@Inject(SESSION_STORAGE) private storage:StorageService,private http : HttpClient) { 
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
  }

  saveCategory(category : Category){
    category.profileId = this.loggedInUser.profileId;
    return this.http.post('http://localhost:8080/hfs/settings/saveCategory',category);
  }

}
