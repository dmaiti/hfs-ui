import { HfsBootstrapModule } from './hfs-bootstrap.module';

describe('HfsBootstrapModule', () => {
  let hfsBootstrapModule: HfsBootstrapModule;

  beforeEach(() => {
    hfsBootstrapModule = new HfsBootstrapModule();
  });

  it('should create an instance', () => {
    expect(hfsBootstrapModule).toBeTruthy();
  });
});
