import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hfs-header',
  templateUrl: './hfs-header.component.html',
  styleUrls: ['./hfs-header.component.css']
})
export class HfsHeaderComponent implements OnInit {

  public title : String = "Home Finance System";

  constructor() { }

  ngOnInit() {
  }

}
