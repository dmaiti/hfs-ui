import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsHeaderComponent } from './hfs-header.component';

describe('HfsHeaderComponent', () => {
  let component: HfsHeaderComponent;
  let fixture: ComponentFixture<HfsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
