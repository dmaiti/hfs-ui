import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsDashboardComponent } from './hfs-dashboard.component';

describe('HfsDashboardComponent', () => {
  let component: HfsDashboardComponent;
  let fixture: ComponentFixture<HfsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
