import { Component, OnInit, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { User } from '../../hfs-settings/users/user-entity';

@Component({
  selector: 'app-hfs-dashboard',
  templateUrl: './hfs-dashboard.component.html',
  styleUrls: ['./hfs-dashboard.component.css']
})
export class HfsDashboardComponent implements OnInit {

  loggedInUser : User;

  constructor(@Inject(SESSION_STORAGE) private storage:StorageService) { 
  }

  ngOnInit() {
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
  }

}
