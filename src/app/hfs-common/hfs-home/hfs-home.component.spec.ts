import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsHomeComponent } from './hfs-home.component';

describe('HfsHomeComponent', () => {
  let component: HfsHomeComponent;
  let fixture: ComponentFixture<HfsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
