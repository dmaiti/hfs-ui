import { Component, OnInit, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { User } from '../../hfs-settings/users/user-entity';

@Component({
  selector: 'app-hfs-home',
  templateUrl: './hfs-home.component.html',
  styleUrls: ['./hfs-home.component.css']
})
export class HfsHomeComponent implements OnInit {

  loggedInUser : User;
  
  constructor(@Inject(SESSION_STORAGE) private storage:StorageService) {

   }

  ngOnInit() {
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
    console.log("Init");
    console.log(this.loggedInUser);
  }

}
