import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsFooterComponent } from './hfs-footer.component';

describe('HfsFooterComponent', () => {
  let component: HfsFooterComponent;
  let fixture: ComponentFixture<HfsFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
