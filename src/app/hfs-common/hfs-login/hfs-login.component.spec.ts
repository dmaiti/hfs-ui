import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsLoginComponent } from './hfs-login.component';

describe('HfsLoginComponent', () => {
  let component: HfsLoginComponent;
  let fixture: ComponentFixture<HfsLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
