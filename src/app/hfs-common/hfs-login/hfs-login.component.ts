import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../../hfs-settings/users/user-entity';
import { UserService } from '../../hfs-settings/user.service';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { Router, ActivatedRoute } from '@angular/router';
import { HfsNavbarComponent } from '../hfs-navbar/hfs-navbar.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hfs-login',
  templateUrl: './hfs-login.component.html',
  styleUrls: ['./hfs-login.component.css']
})
export class HfsLoginComponent implements OnInit {

  userName : string;
  password : string;
  loggedUser : User = new User();
  returnUser : User;
  key : string = "LOGGED_IN_USER";
  navbarComponent : HfsNavbarComponent ;
  loginMessage : String;
  
  constructor(@Inject(SESSION_STORAGE) private storage:StorageService,private userService : UserService,
  private router: Router, private route: ActivatedRoute,private location: Location) { 
    this.navbarComponent = new HfsNavbarComponent(this.storage,this.router,this.route,this.location);
  }

  ngOnInit() {
  }

  login(){

        this.loggedUser.userName = this.userName;
        this.loggedUser.password = this.password;

        this.userService.validateUser(this.loggedUser).subscribe((response) => {
            this.returnUser = response;
            if ( this.returnUser == null || this.returnUser === null || typeof(this.returnUser) === 'undefined')
              this.loginMessage = "Wrong user name or password. Please try again."
            else {
              console.log("Correct user : " + this.returnUser);
              this.storage.set(this.key,this.returnUser);
              this.navbarComponent.ngOnInit();
              this.navbarComponent.refresh();
              this.router.navigate(['/dashboard']);
            }
        });
  }
}
