import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HfsFooterComponent } from './hfs-footer/hfs-footer.component';
import { HfsHeaderComponent } from './hfs-header/hfs-header.component';
import { HfsHomeComponent } from './hfs-home/hfs-home.component';
import { HfsNavbarComponent } from './hfs-navbar/hfs-navbar.component';
import { HfsBootstrapModule } from '../hfs-bootstrap/hfs-bootstrap.module';
import { AppRoutingModule } from '../app-routing.module';
import { HfsLoginComponent } from './hfs-login/hfs-login.component';
import { FormsModule } from '@angular/forms';
import {StorageServiceModule} from 'angular-webstorage-service';
import { HfsDashboardComponent } from './hfs-dashboard/hfs-dashboard.component'

@NgModule({
  imports: [
    CommonModule,
    HfsBootstrapModule,
    AppRoutingModule,
    FormsModule,
    StorageServiceModule
  ],
  declarations: [HfsFooterComponent, HfsHeaderComponent, HfsHomeComponent, HfsNavbarComponent, HfsLoginComponent,  HfsDashboardComponent],
  exports : [HfsFooterComponent,HfsHeaderComponent,HfsHomeComponent, HfsNavbarComponent,HfsDashboardComponent]
})
export class HfsCommonModule { }
