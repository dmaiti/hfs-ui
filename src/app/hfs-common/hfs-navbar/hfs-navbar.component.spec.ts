import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfsNavbarComponent } from './hfs-navbar.component';

describe('HfsNavbarComponent', () => {
  let component: HfsNavbarComponent;
  let fixture: ComponentFixture<HfsNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfsNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfsNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
