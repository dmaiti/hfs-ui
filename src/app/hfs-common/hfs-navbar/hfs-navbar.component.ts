import { Component, OnInit, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { User } from '../../hfs-settings/users/user-entity';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-hfs-navbar',
  templateUrl: './hfs-navbar.component.html',
  styleUrls: ['./hfs-navbar.component.css']
})
export class HfsNavbarComponent implements OnInit {

  loggedInUser : User = new User();
  
  constructor(@Inject(SESSION_STORAGE) private storage:StorageService,private router: Router, private route: ActivatedRoute,
  private location: Location) {
   }

  ngOnInit() {
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
  }

  refresh(){
    location.reload();
  }

  logout(){
    this.storage.remove("LOGGED_IN_USER");
    this.refresh();
    this.router.navigate(['/home']);
  }

}
