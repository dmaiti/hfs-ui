import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { User } from './hfs-settings/users/user-entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loggedInUser : User;

  constructor(@Inject(SESSION_STORAGE) private storage:StorageService,private router: Router, private route: ActivatedRoute) {
   }

   ngOnInit() {
    this.loggedInUser = this.storage.get("LOGGED_IN_USER");
    if ( this.loggedInUser == null || this.loggedInUser === null || typeof(this.loggedInUser) === 'undefined')
      this.router.navigate(['/home']);
  }

}
