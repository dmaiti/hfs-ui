import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './hfs-settings/users/users.component';
import { UserMaintenanceComponent } from './hfs-settings/user-maintenance/user-maintenance.component';
import { HfsHomeComponent } from './hfs-common/hfs-home/hfs-home.component';
import { HfsLoginComponent } from './hfs-common/hfs-login/hfs-login.component';
import { CategoryComponent } from './hfs-settings/category/category.component';
import { HfsNavbarComponent } from './hfs-common/hfs-navbar/hfs-navbar.component';
import { HfsHeaderComponent } from './hfs-common/hfs-header/hfs-header.component';
import { AppComponent } from './app.component';
import { HfsDashboardComponent } from './hfs-common/hfs-dashboard/hfs-dashboard.component';

const routes: Routes = [
  { path: 'createUser', component: UsersComponent },
  { path: 'maintainUser', component: UserMaintenanceComponent },
  { path: 'home', component: HfsHomeComponent },
  { path: 'login', component: HfsLoginComponent },
  { path: 'navbar', component: HfsNavbarComponent },
  { path: 'createCategory', component: CategoryComponent },
  { path: 'dashboard', component: HfsDashboardComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
